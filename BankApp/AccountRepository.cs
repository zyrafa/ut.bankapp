﻿namespace BankApp
{
    public class AccountRepository
    {
        private readonly IDatabaseAccess databaseAccess;

        public AccountRepository(IDatabaseAccess databaseAccess)
        {
            this.databaseAccess = databaseAccess;
        }

        public void Create(IAccount account)
        {
            var existingAccount = Read(account.Number);
            if (existingAccount != null)
                throw new AccountAlreadyExists();

            databaseAccess.Create(account);
        }

        public IAccount Read(string number)
        {
            IAccount account;
            try
            {
                account = databaseAccess.Get(number);
            }
            catch (AccountNotFound)
            {
                return null;
            }

            return account;
        }

        public void Save(IAccount account)
        {
            var existingAccount = Read(account.Number);
            if (existingAccount == null)
                throw new AccountDoesNotExist();

            if (account.Balance == existingAccount.Balance)
                return;

            databaseAccess.Save(account);
        }

    }
}
