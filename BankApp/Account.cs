﻿using System;

namespace BankApp
{
    public class Account : IAccount
    {
        private double balance;

        public string Owner { get; }
        public string Number { get; }
        public double Balance => balance;

        public Account(string owner, string number)
        {
            balance = 0;
            AssertNotEmptyString(owner, "Owner");
            Owner = owner;
            AssertNotEmptyString(number, "Account number");
            Number = number;
        }

        public Account(string owner, string number, double initialFunds)
            : this (owner, number)
        {
            balance = initialFunds;
        }

        public double Deposit(double amount)
        {
            AssertPositiveValue(amount);

            balance += amount;
            return balance;
        }

        public double Withdraw(double amount)
        {
            AssertPositiveValue(amount);
            AssertEnoughFunds(amount);

            balance -= amount;
            return balance;
        }

        private void AssertNotEmptyString(string value, string name)
        {
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentNullException($"{name} cannot be null, empty or whitespace");
        }

        private void AssertPositiveValue(double amount)
        {
            if (amount < 0)
                throw new ArgumentException("Cannot deposit negative amount of money.");
        }

        private void AssertEnoughFunds(double amount)
        {
            if (amount > balance)
                throw new ArgumentException("Insufficient funds on the account to perform operation.");
        }
    }
}
