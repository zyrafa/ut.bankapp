﻿namespace BankApp
{
    public interface IAccount
    {
        double Balance { get; }
        string Number { get; }
        string Owner { get; }

        double Deposit(double amount);
        double Withdraw(double amount);
    }
}