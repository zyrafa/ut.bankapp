﻿namespace BankApp
{
    public interface IDatabaseAccess
    {
        IAccount Get(string number);
        void Create(IAccount account);
        void Save(IAccount account);
    }
}